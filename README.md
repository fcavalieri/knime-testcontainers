# KNIME TestContainers

## Development
Unfortunately, Knime nodes requires use of Maven and Eclipse RCP/RAP (yuck).

### Prerequisites
* Install Java 11 JDK Temurin
* Install Maven (latest release)
* Install Eclipse for RCP and RAP developers (latest release)

Notes:
* Consider using SDKMan for installing Maven and JDKs
* Temurin is recommended under Windows and strongly recommended under Linux. Under Linux openjdk 11 produces errors such as `Inconsistency detected by ld.so: dl-lookup.c: 105: check_match: Assertion 'version->filename == NULL || ! _dl_name_match_p (version->filename, map)' failed!` in many UI interactions, for instance clicking Configure->Browse in a CSV Reader.

### Import in Eclipse
* Import this repo as an existing maven project. Either (File > Import > Maven > Existing Project) or (Project Explorer > Import Projects > Maven > Existing Project)
* Configure/Activate/Reload the installed Java 11 JDK Temurin inside Eclipse (Window > Preferences > Java > Installed JREs)
* Activate target platform by either (Window > Preferences > Plug-in Development > Target Platform and selecting one of the .../KNIME-AP.target targets) or (Expand targetplatform project > KNIME-AP.target > Set as target platform). Note that it might take a while for the target platform to load. Once the target platform is loaded the code should build (0 errors, there could be warnings)

### Diagnosing compilation errors
Never assume Eclipse is working properly. Trust only maven, to an extent. 

Verify that the code builds using Maven from the command line:
* Make sure that the Java JDK used is Java 11 Temurin (`java --version`)
* Package using Maven (`mvn package`)

If the process succeeds, the problem is in Eclipse. If you do not know what you are doing you can try:

Try A:
* Repeat the steps under "Import in Eclipse"

Try B:
* Project > Clean

Try C:
* Right click project root > Refresh
* Right click project root > Maven > Update Project
* Project > Clean

Try D:
* Right click project root > Delete > Select "Delete X nested projects"
* Repeat the steps Import in Eclipse

### Launching KNIME
* For General Use: Right click the launch configuration in the targetplatform project, Run As > KNIME...
* For Integration Tests: Right click the launch configuration in the integration-tests project, Run As > KNIME...

## Repository Structure
* Plugin project: Plugin code
* Targetplatform project: Contains the definitions for the target platform and launch configuration
* Unit-Tests project: JUnit unit tests for the plugin. These must be run from Eclipse. Eventually Tycho will be fixed.
* Integration-Tests project: These are integration tests. They musth be run after a `mvn package` using `mvn verify`. They can be run from Eclipse, but `mvn package` must be run beforehand.
* Dependencies project: Wraps/shades non-trivial dependencies of the plugin
* Feature project: Defines the KNIME Feature that contain the plugin and is published to the P2 repository
* P2 project: Produces the P2 repository that contains the Feature/Plugin

## Notes

These are multifaceted topics, these notes are only a QUICK and DIRTY overview.

### Tests
Apart from Tycho surefire JUnit tests, KNIME tests are testflows (~workflows with nodes that test the output of other nodes).
Each testflows is stored in the integration-tests project as a folder inside the `src/test/knime` folder.
To create/edit them, start KNIME using the launch configuration inside the `integration-tests` project and create/edit workflows in the LOCAL folder.
To run them, right click the workflow in KNIME and select Run as workflow.
Tests are run in the CI against multiple knime versions.

### Dependencies
To use a dependency in the plugin code it (and all its transitive dependencies) must:
* Have (valid and correct) osgi metadata
* Be published in a p2 repository which is configured in KNIME
Moreover:
* The version requirements specified in the plugin must create no conflicts with other enabled plugins.

In general:
* Limit the number of dependencies to the bare minimum.
* If multiple features in different repositories require common dependencies, consider factoring them out.

Hence, there are three main strategies:

#### Play along osgi/p2:
Add a `Require-Bundle` or `Import-Package` statement in the plugin `META-INF/MANIFEST.MF`.
Pro:
* Clean
* Feasible for simple direct dependencies with limited transitive dependencies
Cons:
* Libraries must be osgi compliant
* Libraries must be retrievable using p2
* Strict version requirements can cause incompatibilities with other plugins. Lax version requirements can cause runtime errors. Some library apis change often, requiring strict version requirements and further increasing the issue. 

Example:
* https://github.com/continental/continental-nodes-for-knime/blob/416d8eec7ba1076bcb235fc871a4ce0cafa7a561/com.continental.knime.xlsformatter/META-INF/MANIFEST.MF

Usage in the wild: very rare

#### Embed library jar in the plugin
Add a library jar to a folder in the plugin code (bypassing maven/osgi/p2). 
Include the library jar in the plugin classpath in the `META-INF/MANIFEST.MF` and `build.properties`

Pro:
* Does not require to deal with osgi/p2
* Any library can be added

Cons:
* Bypasses osgi/p2. Classes that come from the jar files can conflict with the same classes loaded from another plugin. incompatibilities are runtime errors.
* Manual handling of dependencies
* Maven/Eclipse/M2E (yuck) limit automation. 
  - It is possible to automate but only from maven CLI. For instance copy-dependencies cannot be done before the package phase in eclipse.
  - Dependency copy automation needs to be done in a separate project to avoid brittle exclusion rules.

Example:
* https://github.com/3D-e-Chem/knime-kripodb/blob/02cb22cc80041aea4b41ea6fc966a5cf9de0f6d6/plugin/META-INF/MANIFEST.MF
* https://github.com/3D-e-Chem/knime-kripodb/blob/02cb22cc80041aea4b41ea6fc966a5cf9de0f6d6/plugin/build.properties
* https://github.com/3D-e-Chem/knime-kripodb/tree/v3.0.0/plugin/lib

Usage in the wild: 90% of all

#### Shade and embed library jar in the plugin
Create a project with `jar` packaging for the single purpose of dependencies wrapping.
Declare plugin dependencies there.
Use maven-shade plugin to shade and relocate classes.
Use maven-resources-plugin to copy the generated shaded jar to the `plugin/libs` folder.
Include the copied dependencies jar in the plugin classpath in the plugin `META-INF/MANIFEST.MF` and `build.properties`.

Pro:
* Does not require to deal with osgi/p2
* Anything can be added
* Classes that are used by the final users are the same that have been tested.
* All and only the classes that are provided by the plugin are in a shaded namespace, making them immediately apparent.

Cons:
* Relocation must be done careful, as it might break stuff at compile time. Not everything can be relocated.
* In case of very large dependency updates, relocation might need to be updated too.

Notes:
* First develop a node without any relocation.
* Then develop good integration tests
* Finally experiment with relocation, trying to relocate every dependency that is of common use. When in doubt relocate.

Example:
* This repository
Usage in the wild: without relocation, discussed. With relocation, never seen.


