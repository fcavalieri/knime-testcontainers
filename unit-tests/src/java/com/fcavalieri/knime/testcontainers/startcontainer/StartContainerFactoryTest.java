package com.fcavalieri.knime.testcontainers.startcontainer;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class StartContainerFactoryTest {
	
	@Test
    public void testGetNrNodeViews() {
		StartContainerNodeFactory factory = new StartContainerNodeFactory();
		
		int nrviews = factory.getNrNodeViews();
		
		assertEquals(0, nrviews);
	}
}