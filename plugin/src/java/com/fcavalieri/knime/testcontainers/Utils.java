package com.fcavalieri.knime.testcontainers;

public class Utils {
	public static boolean isBlank(String string) {
		return string.chars().allMatch(Character::isWhitespace);
	}
}
