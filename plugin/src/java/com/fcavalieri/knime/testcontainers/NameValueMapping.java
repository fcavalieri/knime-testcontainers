package com.fcavalieri.knime.testcontainers;

import java.util.Map;

import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.workflow.FlowVariable;

import com.fcavalieri.knime.testcontainers.NameValueMappingsSettingsModel.ValueKind;

public class NameValueMapping {
	private String name;
	private String value;
	private ValueKind kind;

	public NameValueMapping(String name, String value, ValueKind kind) {
		this.name = name;
		this.value = value;
		this.kind = kind;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRawValue() {
		return value;
	}
	
	public String getValue(Map<String, FlowVariable> flowVariableMap) throws InvalidSettingsException {
		if (kind == ValueKind.FLOW_VARIABLE) {
			if (!flowVariableMap.containsKey(value))
			{
				throw new InvalidSettingsException(String.format("Parameter %s refers to missing flow variable %s", name, value));					
			}
			return flowVariableMap.get(value).getValueAsString();
		}
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public ValueKind getKind() {
		return kind;
	}

	public void setKind(ValueKind kind) {
		this.kind = kind;
	}

	@Override
	public String toString() {
		return "[name=" + name + ", value=" + value + ", kind=" + kind + "]";
	}
}
