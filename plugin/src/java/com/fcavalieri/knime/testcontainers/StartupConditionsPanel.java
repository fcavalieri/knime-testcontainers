package com.fcavalieri.knime.testcontainers;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;

import com.fcavalieri.knime.testcontainers.StartupConditionSettingsModel.WaitForKind;

public class StartupConditionsPanel {
	private final StartupConditionSettingsModel settingsModelStartupConditions;
	
	private final JRadioButton radioDefault;
	private final JRadioButton radioHealthCheck;
	private final JRadioButton radioHTTP;
	private final JRadioButton radioOneShot;
	
	private final JSpinner timeoutSpinner;
	
    private final JSpinner httpPortSpinner;
    private final JSpinner httpStatusSpinner;
    private final JTextField httpPathText;    
	
	private final JPanel panel;
	
	public StartupConditionsPanel(StartupConditionSettingsModel settingsModelStartupConditions) {		
		this.settingsModelStartupConditions = settingsModelStartupConditions;
		
		radioDefault = new JRadioButton(WaitForKind.DEFAULT.toString());
		radioDefault.setSelected(true);
		radioHealthCheck = new JRadioButton(WaitForKind.HEALTHCHECK.toString());
		radioHTTP = new JRadioButton(WaitForKind.HTTP.toString());
		radioOneShot = new JRadioButton(WaitForKind.ONESHOT.toString());

	    ButtonGroup radioGroup = new ButtonGroup();
	    radioGroup.add(radioDefault);
	    radioGroup.add(radioHealthCheck);
	    radioGroup.add(radioHTTP);
	    radioGroup.add(radioOneShot);
	    
	    JLabel timeoutLabel = new JLabel("Timeout (Seconds): ");
	    timeoutSpinner = new JSpinner(new SpinnerNumberModel(60, 1, 999, 1));
	    JPanel timeoutPanel = new JPanel();
	    timeoutPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
	    timeoutPanel.add(timeoutLabel);
	    timeoutPanel.add(timeoutSpinner);
	    
	    JPanel defaultPanel = new JPanel();	    
	    defaultPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
	    defaultPanel.add(radioDefault);

	    JPanel healthCheckPanel = new JPanel();	    
	    healthCheckPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
	    healthCheckPanel.add(radioHealthCheck);
	    
	    JLabel httpPortLabel = new JLabel("Port: ");
	    httpPortSpinner = new JSpinner(new SpinnerNumberModel(80, 1, 65535, 1));
	    JLabel httpStatusLabel = new JLabel("Status: ");
	    httpStatusSpinner = new JSpinner(new SpinnerNumberModel(200, 1, 999, 1));
	    JLabel httpPathLabel = new JLabel("Path: ");
	    httpPathText = new JTextField("/", 50);
	    
	    JPanel oneShotPanel = new JPanel();	    
	    oneShotPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
	    oneShotPanel.add(radioOneShot);
	    
	    JPanel httpPanel = new JPanel();	    
	    httpPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
	    httpPanel.add(radioHTTP);
	    httpPanel.add(httpPortLabel);
	    httpPanel.add(httpPortSpinner);
	    httpPanel.add(httpStatusLabel);
	    httpPanel.add(httpStatusSpinner);
	    httpPanel.add(httpPathLabel);
	    httpPanel.add(httpPathText);
	    
	    panel = new JPanel();
		panel.setLayout(new GridLayout(0, 1));
		panel.add(timeoutPanel);
		panel.add(defaultPanel);
		panel.add(healthCheckPanel);
		panel.add(httpPanel);
		panel.add(oneShotPanel);
	}

	public void saveSettingsTo(NodeSettingsWO settings) {
		updateSettingsModel();
		settingsModelStartupConditions.saveSettingsTo(settings);
	}

	public void loadSettingsFrom(NodeSettingsRO settings) throws InvalidSettingsException {
		settingsModelStartupConditions.loadSettingsFrom(settings);
		loadFromSettingsModel();
	}
	
	private void loadFromSettingsModel() throws InvalidSettingsException {
		radioDefault.setSelected(false);
		radioHealthCheck.setSelected(false);
		radioHTTP.setSelected(false);
		radioOneShot.setSelected(false);
		
		switch (settingsModelStartupConditions.getWaitForKind()) 
		{
			case DEFAULT:
				radioDefault.setSelected(true);
				break;
			case HEALTHCHECK:
				radioHealthCheck.setSelected(true);
				break;
			case HTTP:
				radioHTTP.setSelected(true);
				break;
			case ONESHOT:
				radioOneShot.setSelected(true);
				break;
			default:
				radioDefault.setSelected(true);
				throw new InvalidSettingsException("Unknown WaitFor condition " + settingsModelStartupConditions.getWaitForKind());
		}
			 
		timeoutSpinner.setValue(settingsModelStartupConditions.getWaitForTimeout());
		
		httpPathText.setText(settingsModelStartupConditions.getWaitForHttpPath());
		httpPortSpinner.setValue(settingsModelStartupConditions.getWaitForHttpPort());
		httpStatusSpinner.setValue(settingsModelStartupConditions.getWaitForHttpStatus());
	}

	private void updateSettingsModel() {
		if (radioDefault.isSelected()) {
			settingsModelStartupConditions.setWaitForKind(WaitForKind.DEFAULT);
		} else if (radioHealthCheck.isSelected()) {
			settingsModelStartupConditions.setWaitForKind(WaitForKind.HEALTHCHECK);
		} else if (radioHTTP.isSelected()) {
			settingsModelStartupConditions.setWaitForKind(WaitForKind.HTTP);
		} else if (radioOneShot.isSelected()) {
			settingsModelStartupConditions.setWaitForKind(WaitForKind.ONESHOT);
		} else {
			settingsModelStartupConditions.setWaitForKind(WaitForKind.DEFAULT);
		}
		
		settingsModelStartupConditions.setWaitForTimeout((int)timeoutSpinner.getValue());
		
		settingsModelStartupConditions.setWaitForHttpPath(httpPathText.getText());
		settingsModelStartupConditions.setWaitForHttpPort((int)httpPortSpinner.getValue());
		settingsModelStartupConditions.setWaitForHttpStatus((int)httpStatusSpinner.getValue());			
	}
	

	public JPanel getPanel() {
		return panel;
	}
}
