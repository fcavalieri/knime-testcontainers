package com.fcavalieri.knime.testcontainers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.workflow.FlowVariable;

public class NameValueMappingsSettingsModel {
	
	private final String namesColumn;
	private final String valuesColumn;
	private final String kindsColumn;
	
	private List<NameValueMapping> mappings;
	
	public NameValueMappingsSettingsModel(String identifier, List<NameValueMapping> mappings) {
		this.namesColumn = identifier + "_names";
		this.valuesColumn = identifier + "_values";
		this.kindsColumn = identifier + "_kinds";		
		this.mappings = new ArrayList<>(mappings);
	}

	public void loadSettingsFrom(NodeSettingsRO settings) throws InvalidSettingsException {
		String[] names = settings.getStringArray(namesColumn, new String[]{});
		String[] values = settings.getStringArray(valuesColumn, new String[]{});
		String[] serializedKinds = settings.getStringArray(kindsColumn, new String[]{});
		ValueKind[] kinds = (ValueKind[])Arrays.asList(serializedKinds).stream().map(k -> ValueKind.fromString(k)).collect(Collectors.toList()).toArray(new ValueKind[0]);
		if (names.length != values.length || names.length != serializedKinds.length)
			throw new InvalidSettingsException("Mismatching element count in " + namesColumn + ", " + valuesColumn + " and " + kindsColumn);
		mappings.clear();
		for (int i=0; i < names.length; ++i)
		{
			mappings.add(new NameValueMapping(names[i], values[i], kinds[i]));
		}
	}
	
	public void saveSettingsTo(NodeSettingsWO settings) {
		String[] names = mappings.stream().map(f -> f.getName()).collect(Collectors.toList()).toArray(new String[0]);
		String[] values = mappings.stream().map(f -> f.getRawValue()).collect(Collectors.toList()).toArray(new String[0]);
		String[] serializedKinds = mappings.stream().map(f -> f.getKind().toString()).collect(Collectors.toList()).toArray(new String[0]);
		settings.addStringArray(namesColumn, names);
		settings.addStringArray(valuesColumn, values);
		settings.addStringArray(kindsColumn, serializedKinds);
	}
	

	public List<NameValueMapping> getMappings() {
		return mappings;
	}

	public void setMappings(List<NameValueMapping> mappings) {
		this.mappings = mappings;
	}

	public enum ValueKind {
	    CONSTANT ("Constant"),
	    MULTILINE_CONSTANT ("Multiline Constant"),
	    FLOW_VARIABLE ("Flow variable");	    

	    private final String name;       

	    private ValueKind(String s) {
	        name = s;
	    }

	    public static ValueKind fromString(String name) {
	        for (ValueKind k : ValueKind.values()) {
	            if (k.name.equals(name)) {
	                return k;
	            }
	        }
	        return null;
	    }
	    
	    public boolean equalsName(String otherName) {
	        return name.equals(otherName);
	    }

	    public String toString() {
	       return this.name;
	    }	
	    
	    static ValueKind[] valuesNoMultiline() {
			return new ValueKind[] { CONSTANT, FLOW_VARIABLE };
		}
	}
	
	@Override
	public String toString() {
		return "[mappings=" + mappings + "]";
	}

	public void validate() {
		// TODO Auto-generated method stub		
	}
}
