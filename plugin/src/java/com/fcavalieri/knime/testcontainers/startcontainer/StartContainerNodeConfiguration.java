package com.fcavalieri.knime.testcontainers.startcontainer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.defaultnodesettings.SettingsModelString;

import com.fcavalieri.knime.testcontainers.NameValueMapping;
import com.fcavalieri.knime.testcontainers.NameValueMappingsSettingsModel;
import com.fcavalieri.knime.testcontainers.StartupConditionSettingsModel;
import com.fcavalieri.knime.testcontainers.Utils;

public class StartContainerNodeConfiguration {
	private static final String CFGKEY_NAME = "name";
	private static final String DEFAULT_NAME = "";
	private final SettingsModelString name = new SettingsModelString(CFGKEY_NAME, DEFAULT_NAME);
	
	private static final String CFGKEY_IMAGE= "image";
	private static final String DEFAULT_IMAGE = "";
	private final SettingsModelString image = new SettingsModelString(CFGKEY_IMAGE, DEFAULT_IMAGE);
	
	private static final String CFGKEY_COMMAND = "command";
	private static final String DEFAULT_COMMAND = "";
	private final SettingsModelString command = new SettingsModelString(CFGKEY_COMMAND, DEFAULT_COMMAND);
	
	private static final String CFGKEY_PORTS = "ports";
	private static final String DEFAULT_PORTS = "";
	private final SettingsModelString rawPorts = new SettingsModelString(CFGKEY_PORTS, DEFAULT_PORTS);
	private List<Integer> ports = new ArrayList<>();
	
	private static final String CFGKEY_ENVIRONMENT_VARIABLES = "environment_variables";
	private static final List<NameValueMapping> DEFAULT_ENVIRONMENT_VARIABLES = new ArrayList<NameValueMapping>();
	private NameValueMappingsSettingsModel environmentVariables = new NameValueMappingsSettingsModel(CFGKEY_ENVIRONMENT_VARIABLES, DEFAULT_ENVIRONMENT_VARIABLES);
	
	private static final String CFGKEY_VOLUMES = "volumes";
	private static final List<NameValueMapping> DEFAULT_VOLUMES = new ArrayList<NameValueMapping>();
	private NameValueMappingsSettingsModel volumes = new NameValueMappingsSettingsModel(CFGKEY_VOLUMES, DEFAULT_VOLUMES);

	private StartupConditionSettingsModel startupConditions = new StartupConditionSettingsModel();
		
	public void loadFrom(NodeSettingsRO settings) throws InvalidSettingsException {
		name.loadSettingsFrom(settings);
		image.loadSettingsFrom(settings);
		command.loadSettingsFrom(settings);
		rawPorts.loadSettingsFrom(settings);		
		
		environmentVariables.loadSettingsFrom(settings);
		volumes.loadSettingsFrom(settings);
		startupConditions.loadSettingsFrom(settings);
		validate();
	}
	
	public void validate() throws InvalidSettingsException {
		if (name.getStringValue() == null || Utils.isBlank(name.getStringValue())) {
			throw new InvalidSettingsException("A non-empty name must be specified.");
		}
		if (image.getStringValue() == null || Utils.isBlank(image.getStringValue())) {
			throw new InvalidSettingsException("A non-empty image must be specified.");
		}
		if (rawPorts.getStringValue() != null && !Utils.isBlank(rawPorts.getStringValue())) {
			try {
				ports = Arrays.asList(rawPorts.getStringValue().split(","))
							.stream()
							.map(p -> p.trim())
							.filter(p -> !p.equals(""))
							.map(p -> Integer.parseInt(p))
							.collect(Collectors.toList());
				for (Integer port: ports) {
					if (port > 65535 || port < 1) {
						throw new InvalidSettingsException("Ports must be a comma-separated list of zero or more port numbers (1-65535), without repetitions.");						
					}
				}
				if (new HashSet<Integer>(ports).size() != ports.size()) {
					throw new InvalidSettingsException("Ports must be a comma-separated list of zero or more port numbers (1-65535), without repetitions.");
				}
			} catch (NumberFormatException e) {
				throw new InvalidSettingsException("Ports must be a comma-separated list of zero or more port numbers (1-65535), without repetitions.");
			}
		} else {
			ports = new ArrayList<>();
		}
		
		environmentVariables.validate();
		volumes.validate();
		startupConditions.validate();
	}
	
	public void saveTo(final NodeSettingsWO settings) {
		name.saveSettingsTo(settings);
		image.saveSettingsTo(settings);
		command.saveSettingsTo(settings);
		rawPorts.saveSettingsTo(settings);
		
		environmentVariables.saveSettingsTo(settings);
		volumes.saveSettingsTo(settings);
		startupConditions.saveSettingsTo(settings);
	}

	public SettingsModelString getName() {
		return name;
	}

	public SettingsModelString getImage() {
		return image;
	}

	public SettingsModelString getCommand() {
		return command;
	}

	public SettingsModelString getRawPorts() {
		return rawPorts;
	}
	
    public NameValueMappingsSettingsModel getEnvironmentVariables() {
		return environmentVariables;
	}

	public NameValueMappingsSettingsModel getVolumes() {
		return volumes;
	}
	
	public StartupConditionSettingsModel getStartupConditions() {
		return startupConditions;
	}

	public void setEnvironmentVariables(NameValueMappingsSettingsModel environmentVariables) {
		this.environmentVariables = environmentVariables;
	}

	public void setVolumes(NameValueMappingsSettingsModel volumes) {
		this.volumes = volumes;
	}

	public List<Integer> getPorts() {
		return ports;
	}

	@Override
	public String toString() {
		return "[name=" + name.getStringValue() + ", image=" + image.getStringValue() + ", command=" + command.getStringValue()
				+ ", ports=" + ports + " ("+ rawPorts.getStringValue() +"), environmentVariables=" + environmentVariables
				+ ", volumes=" + volumes + ", startupConditions=" + startupConditions + "]";
	}
}
