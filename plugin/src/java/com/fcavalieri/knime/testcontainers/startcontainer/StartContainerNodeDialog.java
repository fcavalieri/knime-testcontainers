package com.fcavalieri.knime.testcontainers.startcontainer;

import org.knime.core.data.DataTableSpec;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.NotConfigurableException;
import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentString;

import com.fcavalieri.knime.testcontainers.NameValueMappingsPanel;
import com.fcavalieri.knime.testcontainers.StartupConditionsPanel;

public class StartContainerNodeDialog extends DefaultNodeSettingsPane {
	private StartContainerNodeConfiguration config;
	
	private StartupConditionsPanel startupConditionsPanel;
	private NameValueMappingsPanel environmentVariablesPanel;
	private NameValueMappingsPanel volumeMappingsPanel;
	
	protected StartContainerNodeDialog() {
		super();
		setDefaultTabTitle("Container");
		config = new StartContainerNodeConfiguration();
		addDialogComponent(new DialogComponentString(config.getName(), "Name", true, 50));
		addDialogComponent(new DialogComponentString(config.getImage(), "Image", true, 50));
		addDialogComponent(new DialogComponentString(config.getCommand(), "Command", false, 50));
		addDialogComponent(new DialogComponentString(config.getRawPorts(), "Ports", false, 50));
		
		startupConditionsPanel = new StartupConditionsPanel(config.getStartupConditions());    	
		addTab("Startup Conditions", startupConditionsPanel.getPanel());
		
		environmentVariablesPanel = new NameValueMappingsPanel(config.getEnvironmentVariables(), "Name", "Value", true);    	
		addTab("Environment Variables", environmentVariablesPanel.getPanel());
		
		volumeMappingsPanel = new NameValueMappingsPanel(config.getVolumes(), "Container Mount Point", "Host Path", false);
		addTab("Volume Mappings", volumeMappingsPanel.getPanel());
	}

	@Override
	public void loadAdditionalSettingsFrom(NodeSettingsRO settings, DataTableSpec[] specs)
			throws NotConfigurableException {
		super.loadAdditionalSettingsFrom(settings, specs);
		try {
			environmentVariablesPanel.loadSettingsFrom(settings);
		} catch (InvalidSettingsException ignored) {
			//Swallow
		}
		try {
			volumeMappingsPanel.loadSettingsFrom(settings);
		} catch (InvalidSettingsException ignored) {
			//Swallow
		}
		try {
			startupConditionsPanel.loadSettingsFrom(settings);
		} catch (InvalidSettingsException ignored) {
			//Swallow
		}
	}

	@Override
	public void saveAdditionalSettingsTo(NodeSettingsWO settings) throws InvalidSettingsException {
		super.saveAdditionalSettingsTo(settings);
		environmentVariablesPanel.saveSettingsTo(settings);
		volumeMappingsPanel.saveSettingsTo(settings);
		startupConditionsPanel.saveSettingsTo(settings);
	}	
}

