package com.fcavalieri.knime.testcontainers.startcontainer;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import org.knime.core.data.DataTableSpec;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeLogger;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.workflow.NodeContainer;
import org.knime.core.node.workflow.NodeContext;
import org.knime.core.node.workflow.NodeID;
import org.knime.core.node.workflow.WorkflowManager;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.OutputFrame;
import org.testcontainers.containers.startupcheck.OneShotStartupCheckStrategy;
import org.testcontainers.containers.wait.strategy.Wait;

import com.fcavalieri.knime.testcontainers.NameValueMapping;
import com.fcavalieri.knime.testcontainers.StartupConditionSettingsModel;
import com.fcavalieri.knime.testcontainers.Utils;

public class StartContainerNodeModel extends NodeModel {
	private static final NodeLogger LOGGER = NodeLogger.getLogger(StartContainerNodeModel.class);
	
	protected StartContainerNodeConfiguration configuration = new StartContainerNodeConfiguration();
	
	private GenericContainer<?> container;
	
	protected StartContainerNodeModel() {
		super(0, 0);
	}

    /**
	 * {@inheritDoc}
	 */
	@Override
	protected DataTableSpec[] configure(final DataTableSpec[] inSpecs) throws InvalidSettingsException {
		configuration.validate();
		
		List<Integer> ports = configuration.getPorts();
		String containerName = configuration.getName().getStringValue();
		pushFlowVariableString("container_" + containerName + "_id", "--CONTAINER-NOT-STARTED--");
		pushFlowVariableString("container_" + containerName + "_ip_address", "--CONTAINER-NOT-STARTED--");
		pushFlowVariableString("container_" + containerName + "_name", "--CONTAINER-NOT-STARTED--");
		pushFlowVariableString("container_" + containerName + "_image", "--CONTAINER-NOT-STARTED--");
		pushFlowVariableString("container_" + containerName + "_host", "--CONTAINER-NOT-STARTED--");
		pushFlowVariableString("container_" + containerName + "_network_mode", "--CONTAINER-NOT-STARTED--");
		pushFlowVariableString("container_" + containerName + "_gateway", "--CONTAINER-NOT-STARTED--");
		if (ports != null) {
			for (Integer port: ports) {
				pushFlowVariableInt("container_" + containerName + "_port_" + port, 0);
			}
		}
		pushFlowVariableString("container_" + containerName + "_logs", "--CONTAINER-NOT-STARTED--");
		return new DataTableSpec[] {};
	}
    
	/**
	 * 
	 * {@inheritDoc}
	 */
	@Override
	protected BufferedDataTable[] execute(final BufferedDataTable[] inData, final ExecutionContext exec)
			throws Exception {
		LOGGER.warn("Node " + getNodeID() + " is starting a new container with configuration " + configuration);
		
		String image = configuration.getImage().getStringValue();
		container = new GenericContainer<>(image);
		container.withLogConsumer(containerLogsConsumer());
		
		String command = configuration.getCommand().getStringValue();
		if (command != null && !Utils.isBlank(command)) {
			container.setCommand(command);
		}
		
		List<Integer> ports = configuration.getPorts();
		if (ports != null && !ports.isEmpty()) {
			container.setExposedPorts(ports);
		}
		
		StartupConditionSettingsModel startupConditionSettings = configuration.getStartupConditions();
		switch (startupConditionSettings.getWaitForKind()) {
			case DEFAULT:
				container.waitingFor(Wait.forListeningPort().withStartupTimeout(Duration.ofSeconds(startupConditionSettings.getWaitForTimeout())));
				break;
			case HEALTHCHECK:
				container.waitingFor(Wait.forHealthcheck().withStartupTimeout(Duration.ofSeconds(startupConditionSettings.getWaitForTimeout())));
				break;
			case HTTP:
				container.waitingFor(Wait
						.forHttp(startupConditionSettings.getWaitForHttpPath())
						.forPort(startupConditionSettings.getWaitForHttpPort())
						.forStatusCode(startupConditionSettings.getWaitForHttpStatus())
						.withStartupTimeout(Duration.ofSeconds(startupConditionSettings.getWaitForTimeout())));
				break;
			case ONESHOT:
				container.withStartupCheckStrategy(new OneShotStartupCheckStrategy());
				break;
			default:
				throw new InvalidSettingsException("Unknown wait for kind " + startupConditionSettings.getWaitForKind());
		}
		
		List<NameValueMapping> environmentVariables = configuration.getEnvironmentVariables().getMappings();
		for (NameValueMapping environmentVariable: environmentVariables) {
			container.addEnv(environmentVariable.getName(), environmentVariable.getValue(getAvailableInputFlowVariables()));
		}
		
		List<NameValueMapping> volumes = configuration.getVolumes().getMappings();
		for (NameValueMapping volume: volumes) {
			container.addFileSystemBind(volume.getValue(getAvailableInputFlowVariables()), volume.getName(), BindMode.READ_WRITE);
		}
		
		container.start();
		LOGGER.warn("Container " + container.getContainerName() + " for " + container.getDockerImageName() + " (" + container.getContainerId() + ") has started successfully.");
		
		String containerName = configuration.getName().getStringValue();
		pushFlowVariableString("container_" + containerName + "_id", container.getContainerId());
		pushFlowVariableString("container_" + containerName + "_ip_address", container.getContainerIpAddress());
		pushFlowVariableString("container_" + containerName + "_name", container.getContainerName());
		pushFlowVariableString("container_" + containerName + "_image", container.getDockerImageName());
		pushFlowVariableString("container_" + containerName + "_host", container.getHost());
		pushFlowVariableString("container_" + containerName + "_network_mode", container.getNetworkMode());		
		pushFlowVariableString("container_" + containerName + "_gateway", container.getContainerInfo().getNetworkSettings().getGateway());
		if (ports != null) {
			for (Integer port: ports) {
				pushFlowVariableInt("container_" + containerName + "_port_" + port, container.getMappedPort(port));
			}
		}
		pushFlowVariableString("container_" + containerName + "_logs", container.getLogs());
		return new BufferedDataTable[] {};
	}

    public static Consumer<OutputFrame> containerLogsConsumer() {
        return (OutputFrame outputFrame) -> {
            switch (outputFrame.getType()) {
                case STDERR:
                    LOGGER.warn(outputFrame.getUtf8String());
                    break;
                case STDOUT:
                case END:
                	LOGGER.warn(outputFrame.getUtf8String());
                    break;
                default:
                	LOGGER.warn(outputFrame.getUtf8String());
                    break;
            }
        };
    }
    
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveSettingsTo(final NodeSettingsWO settings) {
		configuration.saveTo(settings);		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadValidatedSettingsFrom(final NodeSettingsRO settings) throws InvalidSettingsException {	
		StartContainerNodeConfiguration newConfiguration = new StartContainerNodeConfiguration();
		newConfiguration.loadFrom(settings);
		this.configuration = newConfiguration;
		
		dispose();
		//This is a special node that when a workflow is loaded, reset itself.
		selfReset();	    
	}

	private void dispose() {
		if (container != null && container.isRunning()) {
			LOGGER.warn("Stopping running container " + container.getContainerName() + " for " + container.getDockerImageName() + " (" + container.getContainerId() + ").");
			container.close();
			container = null;
		}
	}
	
	private NodeID getNodeID() {
		NodeContainer nodeContainer = NodeContext.getContext().getNodeContainer();
        WorkflowManager workflowManager = (WorkflowManager) nodeContainer.getDirectNCParent();
        Optional<NodeID> nodeId = workflowManager.findNodes(StartContainerNodeModel.class, false)
        			   .entrySet()
        			   .stream()
        			   .filter(entry -> entry.getValue() == this)
        			   .map(entry -> entry.getKey())
        			   .findFirst();
        if (nodeId.isPresent()) {
        	return nodeId.get();
        } else {
        	throw new IllegalStateException("Cannot find own node id");
        }
	}

	private void selfReset() {
		NodeID nodeID = getNodeID();
		NodeContainer nodeContainer = NodeContext.getContext().getNodeContainer();
		WorkflowManager workflowManager = (WorkflowManager) nodeContainer.getDirectNCParent();
		if (nodeContainer.getNodeContainerState().isExecuted()) {
			new Thread(() -> { 
				LOGGER.warn("Resetting node " + nodeID);
				workflowManager.resetAndConfigureNode(nodeID);				
			}, "Reset node " + nodeID).start();			
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void validateSettings(final NodeSettingsRO settings) throws InvalidSettingsException {
		new StartContainerNodeConfiguration().loadFrom(settings);		
	}

	@Override
	protected void loadInternals(File nodeInternDir, ExecutionMonitor exec)
			throws IOException, CanceledExecutionException {
	}

	@Override
	protected void saveInternals(File nodeInternDir, ExecutionMonitor exec)
			throws IOException, CanceledExecutionException {
	}

	@Override
	protected void reset() {
		dispose();
	}
}

