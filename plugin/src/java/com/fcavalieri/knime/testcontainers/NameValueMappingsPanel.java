package com.fcavalieri.knime.testcontainers;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.knime.core.data.DataTableSpec;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.port.PortObjectSpec;

import com.fcavalieri.knime.testcontainers.NameValueMappingsSettingsModel.ValueKind;

public class NameValueMappingsPanel {	
	public static class JTextAreaEditor extends AbstractCellEditor implements TableCellEditor
	{
		private JTextArea textarea = new JTextArea();

		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
		{
			textarea.setText(value != null ? value.toString() : "");
			return new JScrollPane(textarea);
		}

		public Object getCellEditorValue()
		{
			return textarea.getText();
		}
	}
	
	public static class JTextAreaRenderer implements TableCellRenderer
	{
		private JTextArea textarea = new JTextArea();

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
				int row, int column)
		{
			textarea.setText(value != null ? value.toString() : "");
			return new JScrollPane(textarea);
		}
	}
	
	private final NameValueMappingsSettingsModel settingsModelNameValueMappings;
	private final String nameColumnHeader;
	private final String valueColumnHeader;
	
	private final JTable table;
	private final JPanel panel;
	
	private final int rowHeight;
	private final int multilineRowHeight;

	public NameValueMappingsPanel(NameValueMappingsSettingsModel settingsModelNameValueMappings, String nameColumnHeader, String valueColumnHeader, boolean allowMultiline) {		
		this.settingsModelNameValueMappings = settingsModelNameValueMappings;
		this.nameColumnHeader = nameColumnHeader;
		this.valueColumnHeader = valueColumnHeader;

		table = new JTable() {
			private static final long serialVersionUID = 1L;
			
			public TableCellRenderer getCellRenderer(int row, int column) {
				if (column == 1 && allowMultiline) {
					ValueKind rowKind = (ValueKind) table.getValueAt(row, 2);
					if (rowKind == ValueKind.MULTILINE_CONSTANT) {
						table.setRowHeight(row, multilineRowHeight);
						return new JTextAreaRenderer();
					} else {
						table.setRowHeight(row, rowHeight);
					}
				}
				return super.getCellRenderer(row, column);
			}

			public TableCellEditor getCellEditor(int row, int column) {
				if (column == 1 && allowMultiline) {
					ValueKind rowKind = (ValueKind) table.getValueAt(row, 2);
					if (rowKind == ValueKind.MULTILINE_CONSTANT) {					
						return new JTextAreaEditor();
					}
				} else if (column == 2) {
					ValueKind[] allowedValues = allowMultiline ? ValueKind.values() : ValueKind.valuesNoMultiline();
					JComboBox<ValueKind> kindComboBox = new JComboBox<ValueKind>(allowedValues);
					kindComboBox.addActionListener (new ActionListener () {
					    public void actionPerformed(ActionEvent e) {
					        if (kindComboBox.getSelectedItem() == ValueKind.MULTILINE_CONSTANT) {
					        	table.setRowHeight(row, multilineRowHeight);
					        } else {
					        	table.setRowHeight(row, rowHeight);	
					        }
					    }
					});
					return new DefaultCellEditor(kindComboBox);
				}
				DefaultCellEditor singleclick = new DefaultCellEditor(new JTextField());
			    singleclick.setClickCountToStart(1);
				return singleclick;
			}
		};
		
		rowHeight = table.getRowHeight();
		multilineRowHeight = rowHeight * 5;

		Object[] columns = { nameColumnHeader, valueColumnHeader, "Kind" };
		DefaultTableModel model = new DefaultTableModel();
		model.setColumnIdentifiers(columns);
		table.setModel(model);

		JButton btnAdd = new JButton("Add");
		JButton btnDelete = new JButton("Delete");

		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				model.addRow(new Object[] { null, null, ValueKind.CONSTANT });
			}
		});

		btnDelete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				int i = table.getSelectedRow();
				if (i >= 0) {
					model.removeRow(i);
				} else if (table.getRowCount() > 0) {
					model.removeRow(table.getRowCount() - 1);
				}
			}
		});

		JScrollPane tableScroller = new JScrollPane(table);
		tableScroller.setPreferredSize(new Dimension(250, 80));
		JPanel listPane = new JPanel();
		listPane.setLayout(new BoxLayout(listPane, BoxLayout.PAGE_AXIS));
		// JLabel label = new JLabel("Filters:");
		// label.setLabelFor(table);
		// listPane.add(label);
		// listPane.add(Box.createRigidArea(new Dimension(0,5)));
		listPane.add(tableScroller);
		// listPane.add(Box.createRigidArea(new Dimension(0,5)));
		// listPane.add(open);
		listPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.LINE_AXIS));
		buttonPane.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
		buttonPane.add(Box.createHorizontalGlue());
		buttonPane.add(btnAdd);
		buttonPane.add(Box.createRigidArea(new Dimension(10, 0)));
		buttonPane.add(btnDelete);

		panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.add(listPane, BorderLayout.CENTER);
		panel.add(buttonPane, BorderLayout.PAGE_END);
	}

	public void saveSettingsTo(NodeSettingsWO settings) {
		updateSettingsModel();
		settingsModelNameValueMappings.saveSettingsTo(settings);
	}

	public void loadSettingsFrom(NodeSettingsRO settings) throws InvalidSettingsException {
		settingsModelNameValueMappings.loadSettingsFrom(settings);
		loadFromSettingsModel();
	}
	
	private void loadFromSettingsModel() {
		Object[] columns = { nameColumnHeader, valueColumnHeader, "Kind" };
		DefaultTableModel model = new DefaultTableModel();
		model.setColumnIdentifiers(columns);	
		for (int row = 0; row < settingsModelNameValueMappings.getMappings().size(); ++row) {
			NameValueMapping mapping = settingsModelNameValueMappings.getMappings().get(row);
			model.addRow(new Object[] { mapping.getName(), mapping.getRawValue(), mapping.getKind() });
			//table.setRowHeight(row, mapping.getKind() == ValueKind.MULTILINE_CONSTANT ? multilineRowHeight : rowHeight);
		}		
		table.setModel(model);		
	}

	private void updateSettingsModel() {
		TableCellEditor currentCellEditor = table.getCellEditor();
		if (currentCellEditor != null) {
			try {
				currentCellEditor.stopCellEditing();
			} catch (Exception e) {
				//swallow
			}
		}
		DefaultTableModel model = (DefaultTableModel) table.getModel();		
		ArrayList<NameValueMapping> nameValueMappings = new ArrayList<NameValueMapping>();
		for (int row = 0; row < model.getRowCount(); row++) {			
			NameValueMapping nameValueMapping = new NameValueMapping((String) model.getValueAt(row, 0),
					(String) model.getValueAt(row, 1), (ValueKind) model.getValueAt(row, 2));

			if (nameValueMapping.getName() == null)
				continue;

			nameValueMappings.add(nameValueMapping);
		}
		settingsModelNameValueMappings.setMappings(nameValueMappings);
	}
	
	public JPanel getPanel() {
		return panel;
	}
}
