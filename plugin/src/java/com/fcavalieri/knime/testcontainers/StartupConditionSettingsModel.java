package com.fcavalieri.knime.testcontainers;

import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;

public class StartupConditionSettingsModel {	
	private static final String CFGKEY_WAITFOR_KIND = "waitfor_kind";
	
	private static final String CFGKEY_WAITFOR_HTTP_PORT = "waitfor_http_port";
	private static final String CFGKEY_WAITFOR_HTTP_PATH = "waitfor_http_path";
	private static final String CFGKEY_WAITFOR_HTTP_STATUS = "waitfor_http_status";
	
	private static final String CFGKEY_WAITFOR_LISTENINGPORT_PORT = "waitfor_listeningport_port";
	
	private static final String CFGKEY_WAITFOR_TIMEOUT = "waitfor_timeout";
	
	private WaitForKind waitForKind = WaitForKind.DEFAULT;
	private int waitForTimeout = 60;
	
	private int waitForHttpPort = 80;
	private String waitForHttpPath = "/";
	private int waitForHttpStatus = 200;
	
	private int waitForListeningPortPort = 80;
	
	public void loadSettingsFrom(NodeSettingsRO settings) throws InvalidSettingsException {
		waitForKind = WaitForKind.fromString(settings.getString(CFGKEY_WAITFOR_KIND));
		waitForTimeout = settings.getInt(CFGKEY_WAITFOR_TIMEOUT);

		waitForHttpPort = settings.getInt(CFGKEY_WAITFOR_HTTP_PORT);
		waitForHttpPath = settings.getString(CFGKEY_WAITFOR_HTTP_PATH);
		waitForHttpStatus = settings.getInt(CFGKEY_WAITFOR_HTTP_STATUS);
		
		waitForListeningPortPort = settings.getInt(CFGKEY_WAITFOR_LISTENINGPORT_PORT);
	}
	
	public void saveSettingsTo(NodeSettingsWO settings) {
		settings.addString(CFGKEY_WAITFOR_KIND, waitForKind.toString());
		settings.addInt(CFGKEY_WAITFOR_TIMEOUT, waitForTimeout);
		
		settings.addInt(CFGKEY_WAITFOR_HTTP_PORT, waitForHttpPort);
		settings.addString(CFGKEY_WAITFOR_HTTP_PATH, waitForHttpPath);
		settings.addInt(CFGKEY_WAITFOR_HTTP_STATUS, waitForHttpStatus);
		
		settings.addInt(CFGKEY_WAITFOR_LISTENINGPORT_PORT, waitForListeningPortPort);		
	}

	public enum WaitForKind {
		DEFAULT("Default"),
		HEALTHCHECK("Health Check"), 
		HTTP("HTTP"),
		ONESHOT("One Shot");
		
		private final String name;       

	    private WaitForKind(String s) {
	        name = s;
	    }

	    public static WaitForKind fromString(String name) {
	        for (WaitForKind k : WaitForKind.values()) {
	            if (k.name.equals(name)) {
	                return k;
	            }
	        }
	        return null;
	    }
		
	    public boolean equalsName(String otherName) {
	        return name.equals(otherName);
	    }

	    public String toString() {
	       return this.name;
	    }
	}
	
	public WaitForKind getWaitForKind() {
		return waitForKind;
	}

	public void setWaitForKind(WaitForKind waitForKind) {
		this.waitForKind = waitForKind;
	}

	public int getWaitForTimeout() {
		return waitForTimeout;
	}

	public void setWaitForTimeout(int waitForTimeout) {
		this.waitForTimeout = waitForTimeout;
	}

	public int getWaitForHttpPort() {
		return waitForHttpPort;
	}

	public void setWaitForHttpPort(int waitForHttpPort) {
		this.waitForHttpPort = waitForHttpPort;
	}

	public String getWaitForHttpPath() {
		return waitForHttpPath;
	}

	public void setWaitForHttpPath(String waitForHttpPath) {
		this.waitForHttpPath = waitForHttpPath;
	}

	public int getWaitForHttpStatus() {
		return waitForHttpStatus;
	}

	public void setWaitForHttpStatus(int waitForHttpStatus) {
		this.waitForHttpStatus = waitForHttpStatus;
	}

	public int getWaitForListeningPortPort() {
		return waitForListeningPortPort;
	}

	public void setWaitForListeningPortPort(int waitForListeningPortPort) {
		this.waitForListeningPortPort = waitForListeningPortPort;
	}

	@Override
	public String toString() {
		return "[waitForKind=" + waitForKind + ", waitForTimeout=" + waitForTimeout
				+ ", waitForHttpPort=" + waitForHttpPort + ", waitForHttpPath=" + waitForHttpPath
				+ ", waitForHttpStatus=" + waitForHttpStatus + ", waitForListeningPortPort=" + waitForListeningPortPort
				+ "]";
	}

	public void validate() {
	
	}
}
